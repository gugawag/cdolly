package cdolly.examples;

import cdolly.executor.Executor;

public class RunOnlyWithParams {

	public static void main(String[] args) {
		String alloyModelPath = "alloy/run_singleOnlyWithParams.als";
		Executor executor = Executor.createExecutor(alloyModelPath);
		executor.run();
	}

}
