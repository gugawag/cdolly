package cdolly.examples;

import cdolly.executor.Executor;

public class RunSingleTranslationUnit {

	public static void main(String[] args) {
		String alloyModelPath = "alloy/run_singleTranslationUnit.als";
		Executor executor = Executor.createExecutor(alloyModelPath);
		executor.run();
	}

}
