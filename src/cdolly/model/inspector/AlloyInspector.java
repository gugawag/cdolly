package cdolly.model.inspector;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import cdolly.model.Entity;
import cdolly.model.Relation;
import edu.mit.csail.sdg.alloy4.SafeList;
import edu.mit.csail.sdg.alloy4compiler.ast.Sig;
import edu.mit.csail.sdg.alloy4compiler.ast.Sig.Field;
import edu.mit.csail.sdg.alloy4compiler.translator.A4Solution;

/**
 * @author Jeanderson Candido<br>
 *         <a target="_blank"
 *         href="http://jeandersonbc.github.io">http://jeandersonbc
 *         .github.io</a>
 * 
 */
public class AlloyInspector implements IModelVisitor {

	private static final String CORE_ID = "core/";
	private A4Solution model;

	/**
	 * @param toBeInspected
	 *            The Alloy instance ({@link A4Solution}) to be inspected.
	 */
	public AlloyInspector(A4Solution toBeInspected) {
		if (toBeInspected != null) {
			this.model = toBeInspected;
		}
	}

	@Override
	public List<String> getTranslationUnits() {
		Sig sig = getSigByEntity(Entity.TRANSLATION_UNIT);
		List<String> names = new ArrayList<String>();
		if (sig != null) {
			String rawNames = this.model.eval(sig).toString();
			for (String name : rawNames.split(",")) {
				names.add(removeBraces(name));
			}
		}
		return names;
	}

	@Override
	public List<String> getGlobalDeclarationsFrom(String translationUnit) {
		return getDeclarationsFrom(translationUnit, Entity.TRANSLATION_UNIT,
				Relation.DECLARES);
	}

	@Override
	public String getFunctionNameFrom(String declaration) {
		return getNameBy(Entity.FUNCTION, declaration);
	}

	@Override
	public List<String> getParametersFrom(String functionDeclaration) {
		return getDeclarationsFrom(functionDeclaration, Entity.FUNCTION,
				Relation.PARAM);
	}

	@Override
	public String getReturnTypeFrom(String declaration) {
		return getTypeFrom(declaration, Entity.FUNCTION, Relation.RETURN_TYPE);
	}

	@Override
	public List<String> getLocalDeclarationsFrom(String functionDeclaration) {
		return getDeclarationsFrom(functionDeclaration, Entity.FUNCTION,
				Relation.DECLARES);
	}

	@Override
	public String getDeclaringTypeFrom(String variableDeclaration,
			boolean isGlobal) {

		return getTypeFrom(variableDeclaration,
				(isGlobal ? Entity.GLOBAL_VARIABLE : Entity.LOCAL_VARIABLE),
				Relation.TYPE);
	}

	@Override
	public IStringChecker getStringChecker() {
		return StringCheckerImpl.getInstance();
	}

	@Override
	public String getVariableNameFrom(String declaration, boolean isGlobal) {
		return getNameBy((isGlobal ? Entity.GLOBAL_VARIABLE
				: Entity.LOCAL_VARIABLE), declaration);
	}

	@Override
	public String getReturnStmtFrom(String declaration) {
		return getTypeFrom(declaration, Entity.FUNCTION, Relation.RETURN_STMT);
	}

	/**
	 * Gets all declarations of <code>targetDeclaration</code> existing in the
	 * <code>belongingEntity</code> with the relationship
	 * <code>relationKind</code>.
	 * <p>
	 * If such declaration or relation does not exist in the belonging entity,
	 * and empty list is returned.
	 * </p>
	 * 
	 * @param targetDeclaration
	 *            The target declaration
	 * @param belongingEntity
	 *            The entity that has the target declaration.
	 * @param relationKind
	 *            The relation in the belonging entity having the target
	 *            declaration.
	 * @return A list of declarations from target declarations.
	 */
	private List<String> getDeclarationsFrom(String targetDeclaration,
			Entity belongingEntity, Relation relationKind) {
		String rawDeclarations = getRawDeclaration(belongingEntity,
				relationKind);
		List<String> declarations = new LinkedList<String>();
		for (String declaration : rawDeclarations.split(", ")) {
			if (declaration.contains(targetDeclaration)) {
				declarations.add(removePrefix(belongingEntity, declaration));
			}
		}
		return declarations;
	}

	private String getTypeFrom(String typedDeclaration, Entity entity,
			Relation relationType) {
		String rawDeclarations = getRawDeclaration(entity, relationType);
		String rawType = "";
		for (String name : rawDeclarations.split(", ")) {
			if (name.contains(typedDeclaration)) {
				rawType = removePrefix(entity, name);
				return rawType.replace(CORE_ID, "").replaceAll("\\$[0-9]*", "");
			}
		}
		return rawType;
	}

	private Field getFieldBy(Entity entity, Relation relation) {
		Sig sig = getSigByEntity(entity);
		SafeList<Field> fields = sig.getFields();
		for (Field f : fields) {
			if (f.toString().contains(entity.label())
					&& f.toString().contains(relation.label()))
				return f;
		}
		throw new FieldNotFoundException(entity, relation);
	}

	private String getNameBy(Entity entity, String declaration) {
		String rawDeclarations = getRawDeclaration(entity, Relation.ID);
		String target = "";
		for (String name : rawDeclarations.split(", ")) {
			if (name.contains(declaration)) {
				target = removePrefix(entity, name);
				return target.replace(CORE_ID, "").replace("$", "_");
			}
		}
		return target;

	}

	private String getRawDeclaration(Entity entity, Relation relation) {
		Field field = getFieldBy(entity, relation);
		return removeBraces(this.model.eval(field).toString());
	}

	private Sig getSigByEntity(Entity kind) {
		if (kind != null) {
			SafeList<Sig> allSigsFromModel = this.model.getAllReachableSigs();
			String pureSigName = null;
			for (Sig sig : allSigsFromModel) {
				pureSigName = sig.toString().replace(CORE_ID, "");
				if (pureSigName.equals(kind.label()))
					return sig;
			}
		}
		throw new SigNotFoundException(kind.label());
	}

	private String removePrefix(Entity entity, String rawDeclarations) {
		StringBuilder regex = new StringBuilder(CORE_ID);
		regex.append(entity.label());
		regex.append("\\$[0-9]+\\->");

		return rawDeclarations.replaceAll(regex.toString(), "");
	}

	private String removeBraces(String name) {
		return name.replace("{", "").replace("}", "");
	}

}
