package cdolly.model;

/**
 * @author Jeanderson Candido<br>
 *         <a href="http://jeandersonbc.github.io"
 *         target="_blank">http://jeandersonbc.github.io</a>
 */
public enum Entity {

	TRANSLATION_UNIT("TranslationUnit"), FUNCTION("Function"), GLOBAL_VARIABLE(
			"GlobalVariable"), LOCAL_VARIABLE("LocalVariable"), DECLARATION(
			"Declaration"), IDENTIFIER("Identifier"), RETURN_STMT("ReturnStmt");

	private String label;

	Entity(String sigLabel) {
		this.label = sigLabel;
	}

	@Override
	public String toString() {
		return this.label;
	}

	/**
	 * 
	 * @return The label for this {@link Entity}.
	 */
	public String label() {
		return toString();
	}

	public boolean isDeclaredBy(String declaration) {
		return (declaration == null ? false : declaration.contains(this.label));
	}
}
