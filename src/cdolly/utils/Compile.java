package cdolly.utils;
import java.io.File;

import org.apache.log4j.Logger;


public class Compile {

	private static Logger logger = Logger.getLogger(Compile.class);
	


	public static boolean run(String programPath) {

		FuncoesGerais.executaComando("cp /Users/gugawag/Documents/mestrado/workspace/cdolly-v2/resources/script_seq.sh "
							+ programPath, null);
		//copying main.c to be possible compile a program
		FuncoesGerais.executaComando("cp /Users/gugawag/Documents/mestrado/workspace/cdolly-v2/resources/main.c "
				+ programPath, null);

		
		File scriptPath = new File(programPath + "/script_seq.sh");
		boolean executouComandoCorretamente = FuncoesGerais.executaComando(scriptPath.getAbsolutePath()
				+ " a.out " + programPath + " " + "c99" + " " + "clang" + " " + " ", null);

		//removing files
		FuncoesGerais.executaComando("rm " + programPath + "/script_seq.sh ", null);
		FuncoesGerais.executaComando("rm " + programPath + "/main.c ", null);
		if (executouComandoCorretamente){
			FuncoesGerais.executaComando("rm " + programPath + "/a.out ", null);
			return true;
		}

        return false;        
	}

}
